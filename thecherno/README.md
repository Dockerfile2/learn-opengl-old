# OpenGL theCherno

## How to Build
Compile using cmake in a subdirectory of the CMakeLists.txt file in order 
to correctly reference the shader file. Beware that there is currently a 
gcc-only function.

### Linux Instructions:
```
mkdir build
cd build
cmake ..
make
```

## Then run the program:
```
./opengl
```
